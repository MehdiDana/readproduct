<?php

/**
 * Class ReadProduct
 */
class ReadProduct
{
    /**
     * @var string
     */
    public $url = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html';

    /** Get product list as json string
     * @return string
     */
    public function getJsonProductList()
    {
        return json_encode($this->getProductList());
    }

    /**
     * Get product list as array
     * @return array
     */
    public function getProductList()
    {
        $html = $this->getCurl($this->url);

        $dom = new DOMDocument();
        @$dom->loadHTML($html);

        $productLister = $dom->getElementById("productLister");
        $xpath = new DOMXpath($dom);
        $products = $xpath->query('//div[@class="productInfo"]', $productLister);

        $productList = [];
        $total = 0.0;
        foreach($products as $product) {
            $productDomLink = $product->getElementsByTagName("a");
            foreach ($productDomLink as $productLink) {
                $href =  $productLink->getAttribute("href");
                $info = $this->getProductInfo($href);
                $productList['results'][] = $info;
                $total += floatval($info['unit_price']);
            }
        }
        $productList['total'] = $total;

        return $productList;
    }

    /**
     * By given url, get the product information
     * @param string $url
     * @return array
     */
    protected function getProductInfo($url)
    {
        $html = $this->getCurl($url);

        $dom = new DOMDocument();
        @$dom->loadHTML($html);

        $page = $dom->getElementById("page");
        $xpath = new DOMXpath($dom);
        $productContent = $xpath->query('//div[@class="section productContent"]', $page);
        $productDom = $productContent->item(0);

        $product = [];

        $productNameDom = $xpath->query('//div[@class="productTitleDescriptionContainer"]', $productDom);
        $product['title'] = trim($productNameDom->item(0)->nodeValue);

        $productPriceDom = $xpath->query('//p[@class="pricePerUnit"]', $productDom);
        $productPrice = trim($productPriceDom->item(0)->nodeValue);
        $product['unit_price']  = preg_replace("/[^0-9.]/","", $productPrice);

        $productInfoDom = $xpath->query('//div[@class="productText"]', $productDom);
        $productDesDom = $xpath->query('//h3[@class="productDataItemHeader"]', $productDom);
        $i = 0;
        foreach ($productDesDom as $productDes) {
            if(trim($productDes->nodeValue) == 'Description') {
                $product['description'] = trim($productInfoDom->item($i)->nodeValue);
            }
            if(trim($productDes->nodeValue) == 'Size') {
                $product['size'] = trim($productInfoDom->item($i)->nodeValue);
            }
            $i++;
        }

        return $product;
    }

    /**
     * Do curl given url
     * @param $url
     * @return mixed
     */
    protected function getCurl($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $html = curl_exec($curl);
        curl_close($curl);

        return $html;
    }

}