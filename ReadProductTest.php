<?php

require_once('ReadProduct.php');

/**
 * Class ReadProductTest
 */
class ReadProductTest extends PHPUnit_Framework_TestCase
{
    /**
     * Test result, the expected result was check manually. this may change thou
     */
    public function testReadProduct() {
        $obj = new ReadProduct();
        $this->assertEquals('{"results":[{"title":"Sainsbury\'s Apricot Ripe & Ready x5","unit_price":"3.50","description":"Apricots","size":"5Count"},{"title":"Sainsbury\'s Avocado Ripe & Ready XL Loose 300g","unit_price":"1.50","description":"Avocados","size":"275g"},{"title":"Sainsbury\'s Avocado, Ripe & Ready x2","unit_price":"1.80","description":"Avocados","size":"2Count"},{"title":"Sainsbury\'s Avocados, Ripe & Ready x4","unit_price":"3.20","description":"Avocados","size":"x4Count"},{"title":"Sainsbury\'s Conference Pears, Ripe & Ready x4 (minimum)","unit_price":"1.50","description":"Conference","size":"4Count"},{"title":"Sainsbury\'s Golden Kiwi x4","unit_price":"1.80","description":"Gold Kiwi","size":"x4"},{"title":"Sainsbury\'s Kiwi Fruit, Ripe & Ready x4","unit_price":"1.80","description":"Kiwi","size":"x4"}],"total":15.1}', $obj->getJsonProductList());
    }

    /**
     * Test the result as array, this expected result checked manually
     */
    public function testGetProductList() {
        $obj = new ReadProduct();

        $dataArray = $obj->getProductList();

        $this->assertCount(2, $dataArray);
        $this->assertCount(7, $dataArray['results']);

        $this->assertArrayHasKey('title', $dataArray['results'][0]);
        $this->assertArrayHasKey('unit_price', $dataArray['results'][0]);
        $this->assertArrayHasKey('description', $dataArray['results'][0]);
        $this->assertArrayHasKey('size', $dataArray['results'][0]);
        $this->assertArrayHasKey('total', $dataArray);
    }

    /**
     * Test the json result
     */
    public function testGetJsonProductList() {
        $obj = new ReadProduct();

        $dataJson = $obj->getJsonProductList();
        $this->assertNotNull($dataJson);

        $dataArray = json_decode($dataJson, true);
        $this->assertArrayHasKey('total', $dataArray);
    }
}
